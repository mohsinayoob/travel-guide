package com.yuvix.tourustan.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.yuvix.tourustan.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import objects.Feedback;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.view.View.GONE;
import static utils.Constants.API_LINK_V2;
import static utils.Constants.USER_TOKEN;

public class BugReportFragment extends Fragment {

    @BindView(R.id.issues_list)
    ListView issuesList;
    @BindView(R.id.add_feedback)
    FloatingActionButton addButton;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.text_view)
    TextView textView;
    @BindView(R.id.listview_title)
    TextView listViewTitle;

    private String mAuthToken = null;
    private final List<Feedback> mFeedbacks = new ArrayList<>();
    private BugReportAdapter mAdapter;
    private Activity mActivity;

    public BugReportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment BugReportFragment.
     */
    public static BugReportFragment newInstance() {
        BugReportFragment fragment = new BugReportFragment();
        return fragment;
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mBugReportView = inflater.inflate(R.layout.fragment_bug_report, container, false);
        ButterKnife.bind(this, mBugReportView);

        mAuthToken = getAuthToken();

        addButton.setOnClickListener(v -> {
            Fragment fragment;
            FragmentManager fragmentManager = getFragmentManager();
            fragment = AddBugFragment.newInstance();
            fragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.inc, fragment).commit();
        });
        return mBugReportView;
    }

    public String getAuthToken() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        return sharedPreferences.getString(USER_TOKEN, null);
    }

    /**
     * fetches a list of feedbacks reported by user from
     * server
     */
    private void getAllUserFeedback() {

    }

    private void noResult() {
        textView.setVisibility(View.VISIBLE);
        listViewTitle.setVisibility(GONE);
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.no_feedback);
        animationView.playAnimation();
    }

    private void networkError() {
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        mFeedbacks.clear();
        getAllUserFeedback();
    }
}