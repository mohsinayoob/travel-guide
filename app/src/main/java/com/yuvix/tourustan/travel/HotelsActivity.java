package com.yuvix.tourustan.travel;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.yuvix.tourustan.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static utils.Constants.API_LINK_V2;
import static utils.Constants.USER_TOKEN;

/**
 * Display list of hotels in destination city
 */
public class HotelsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String KEY_SELECTED_CITY = "KEY_SELECTED_CITY";

    @BindView(R.id.hotel_list)
    RecyclerView recyclerView;
    @BindView(R.id.select_city)
    Button selectCity;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.text_view)
    TextView textView;
    @BindView(R.id.layout)
    LinearLayout layout;

    private Handler mHandler;
    private String mToken;

//    private CitySearchModel mSelectedCity;
//
//    private ArrayList<CitySearchModel> mSearchCities = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotels);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        mHandler = new Handler(Looper.getMainLooper());
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);

        fetchCitiesList();

        setTitle("Hotels");

        selectCity.setOnClickListener(this);

        Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_SELECTED_CITY)) {
//            mSelectedCity = savedInstanceState.getParcelable(KEY_SELECTED_CITY);
//            if (mSelectedCity != null) {
//                selectCity.setText(String.format(getString(R.string.showing_hotels), mSelectedCity.getName()));
//                getCityInfo(mSelectedCity.getId());
//            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    /**
     * Calls API to get hotel list
     */
    private void getHotelList(String latitude, String longitude) {

    }

    /**
     * Fetches the list cities from server
     */
    private void fetchCitiesList() {

    }

    /**
     * Calls the API & fetch details of city with given id
     *
     * @param cityId the city id
     */
    public void getCityInfo(String cityId) {

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.select_city:
//                new CitySearchDialogCompat(HotelsActivity.this, getString(R.string.search_title),
//                        getString(R.string.search_hint), null, mSearchCities,
//                        (SearchResultListener<CitySearchModel>) (dialog, item, position) -> {
//                            mSelectedCity = item;
//                            String selectedCity = item.getId();
//                            selectCity.setText(String.format(getString(R.string.showing_hotels), item.getName()));
//                            dialog.dismiss();
//                            getCityInfo(selectedCity);
//                        }).show();
                recyclerView.setAdapter(null);
                break;
        }
    }


    /**
     * save selected city to bundle
     * in case of configuration change like device screen rotation.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putParcelable(KEY_SELECTED_CITY, mSelectedCity);
    }

    // TODO :: Move adapter to a new class
    public class HotelsAdapter extends RecyclerView.Adapter<HotelsAdapter.HotelsViewHolder> {

        private final Context mContext;
        private List<HotelsModel> mHotelsModelList;

        HotelsAdapter(Context context, List<HotelsModel> mHotelsModelList) {
            this.mContext = context;
            this.mHotelsModelList = mHotelsModelList;
        }

        @NonNull
        @Override
        public HotelsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hotel_listitem, parent, false);
            return new HotelsViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull HotelsViewHolder holder, int position) {

            try {
                holder.title.setText(mHotelsModelList.get(position).getTitle());
                holder.description.setText(android.text.Html.fromHtml(mHotelsModelList.get(position).getAddress()));
                holder.distance.setText(new DecimalFormat("##.##").format((float) mHotelsModelList.get(position)
                        .getDistance() / 1000));
                holder.call.setOnClickListener(view -> {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    try {
                        intent.setData(Uri.parse("tel:" + mHotelsModelList.get(position).getPhone()));
                        mContext.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        networkError();
                    }
                });

                holder.map.setOnClickListener(view -> {
                    Intent browserIntent;
                    try {
                        Double latitude =
                                mHotelsModelList.get(position).getLatitude();
                        Double longitude =
                                mHotelsModelList.get(position).getLongitude();
                        browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps?q=" +
                                mHotelsModelList.get(position).getTitle() +
                                "+(name)+@" + latitude +
                                "," + longitude));
                        mContext.startActivity(browserIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        networkError();
                    }
                });
                holder.book.setOnClickListener(view -> {
                    Intent browserIntent;
                    try {
                        browserIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(mHotelsModelList.get(position).getHref()));
                        mContext.startActivity(browserIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        networkError();
                    }
                });

                holder.expand_more_details.setOnClickListener((View view) -> {
                    if (holder.detailsLayout.getVisibility() == View.GONE) {
                        holder.detailsLayout.setVisibility(View.VISIBLE);
                        holder.expandCollapse.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                    } else {
                        holder.detailsLayout.setVisibility(View.GONE);
                        holder.expandCollapse.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("ERROR : ", "Message : " + e.getMessage());
            }
        }

        @Override
        public int getItemCount() {
            return mHotelsModelList.size();
        }

        //        View holder Class to hold the Views
        class HotelsViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.hotel_name)
            TextView title;
            @BindView(R.id.hotel_address)
            TextView description;
            @BindView(R.id.call)
            RelativeLayout call;
            @BindView(R.id.map)
            RelativeLayout map;
            @BindView(R.id.book)
            RelativeLayout book;
            @BindView(R.id.more_details)
            LinearLayout detailsLayout;
            @BindView(R.id.expand_collapse)
            ImageView expandCollapse;
            @BindView(R.id.distance)
            TextView distance;
            @BindView(R.id.expand_more_details)
            RelativeLayout expand_more_details;

            private HotelsViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

    /**
     * Plays the network lost animation in the view
     */
    private void networkError() {
        layout.setVisibility(View.GONE);
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

    /**
     * Plays the no results animation in the view
     */
    private void noResults() {
        layout.setVisibility(View.GONE);
        animationView.setVisibility(View.VISIBLE);
        Toast.makeText(HotelsActivity.this, R.string.no_trips, Toast.LENGTH_LONG).show();
        animationView.setAnimation(R.raw.empty_list);
        animationView.playAnimation();
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, HotelsActivity.class);
    }

    // TODO :: Move model to a new class
//    Model class to hold the data instead of passing the whole JSONObject to recyclerView
    class HotelsModel {
        private String mTitle;
        private String mAddress;
        private String mPhone;
        private String mHref;
        private int mDistance;
        private double mLatitude;
        private double mLongitude;

        HotelsModel(String mTitle, String mAddress, String mPhone, String mHref, int mDistance,
                    double mLatitude, double mLongitude) {
            this.mTitle = mTitle;
            this.mAddress = mAddress;
            this.mPhone = mPhone;
            this.mHref = mHref;
            this.mDistance = mDistance;
            this.mLatitude = mLatitude;
            this.mLongitude = mLongitude;
        }

        public String getTitle() {
            return mTitle;
        }

        public String getAddress() {
            return mAddress;
        }

        public String getPhone() {
            return mPhone;
        }

        public String getHref() {
            return mHref;
        }

        public int getDistance() {
            return mDistance;
        }

        public double getLatitude() {
            return mLatitude;
        }

        public double getLongitude() {
            return mLongitude;
        }
    }
}
