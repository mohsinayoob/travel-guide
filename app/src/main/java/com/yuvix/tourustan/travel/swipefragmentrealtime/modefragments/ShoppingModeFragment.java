package com.yuvix.tourustan.travel.swipefragmentrealtime.modefragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.airbnb.lottie.LottieAnimationView;
import com.yuvix.tourustan.R;
import com.yuvix.tourustan.travel.swipefragmentrealtime.MapListItemAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import objects.MapItem;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static utils.Constants.API_LINK_V2;
import static utils.Constants.HERE_API_MODES;

public class ShoppingModeFragment extends Fragment {

    @BindView(R.id.listview)
    ListView listView;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;

    private static String mCurlat;
    private static String mCurlon;
    private static String mToken;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private List<MapItem> mMapItems =  new ArrayList<>();

    public ShoppingModeFragment() {
     //required public constructor
    }

    public static ShoppingModeFragment newInstance(String currentLat, String currentLon, String token) {
        mCurlat = currentLat;
        mCurlon = currentLon;
        mToken = token;
        return new ShoppingModeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_each_list_real_time, container, false);
        ButterKnife.bind(this, rootView);
        getPlaces();
        return rootView;
    }

    /**
     * Gets all nearby shopping places
     */
    private void getPlaces() {

    }

    /**
     * Plays the network lost animation in the view
     */
    private void networkError() {
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

    /**
     * Plays the no data found animation in the view
     */
    private void noResult() {
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.empty_list);
        animationView.playAnimation();
    }
}
