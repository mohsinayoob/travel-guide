package com.yuvix.tourustan.travel;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.yuvix.tourustan.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.Objects;

import adapters.PlaceAutocompleteAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import utils.Utils;

import static utils.Constants.API_LINK_V2;
import static utils.Constants.USER_TOKEN;

public class ShoppingCurrentCityActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.shopping_list)
    ListView lv;
    @BindView(R.id.query)
    EditText q;
    @BindView(R.id.go)
    Button ok;
    @BindView(R.id.text_view)
    TextView textView;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.show_city_name)
    TextView showCityName;

    public static FrameLayout searchLayout;

    public static AutoCompleteTextView mSearchView;
    private String mToken;
    private Handler mHandler;

    private GoogleApiClient mGoogleApiClient;
    PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(
            new LatLng(-40, -168), new LatLng(71, 136));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_currentcity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = sharedPreferences.getString(USER_TOKEN, null);
        mHandler = new Handler(Looper.getMainLooper());

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setTitle(getResources().getString(R.string.text_shopping));

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(this, R.layout.autocomplete_list_item);
        mSearchView = findViewById(R.id.search_view);
        mSearchView.setAdapter(mPlaceAutocompleteAdapter);

        searchLayout = findViewById(R.id.search_layout);
//        mSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                //Do some magic
//                Log.v("QUERY ITEM : ", query);
//                try {
//                    getShoppingItems(query);
//                } catch (Exception e) {
//                    networkError();
//                }
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                //Do some magic
//                return false;
//            }
//        });
//
//
//        mSearchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
//            @Override
//            public void onSearchViewShown() {
//                //Do some magic
//            }
//
//            @Override
//            public void onSearchViewClosed() {
//                //Do some magic
//            }
//        });

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @OnClick(R.id.go)
    void onClick() {
        try {
            String item = q.getText().toString();
            getShoppingItems(item);
        } catch (Exception e) {
            networkError();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
//        mSearchView.setMenuItem(item);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                findViewById(R.id.search_layout).setVisibility(View.VISIBLE);
                return true;
            }
        });
        return true;
    }

    @Override
    public void onBackPressed() {
        if (findViewById(R.id.search_layout).getVisibility() == View.VISIBLE) {
//            mSearchView.closeSearch();
            findViewById(R.id.search_layout).setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    private void getShoppingItems(final String item) {

    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ShoppingCurrentCityActivity.class);
        return intent;
    }
    /**
     * Plays the network lost animation in the view
     */
    private void networkError() {
        showCityName.setVisibility(View.GONE);
        animationView.setVisibility(View.VISIBLE);
        layout.setVisibility(View.GONE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }
    /**
     * Plays the no results animation in the view
     */
    private void noResults() {
        animationView.setVisibility(View.VISIBLE);
        layout.setVisibility(View.GONE);
        Toast.makeText(ShoppingCurrentCityActivity.this,  R.string.no_results, Toast.LENGTH_LONG).show();
        animationView.setAnimation(R.raw.empty_list);
        animationView.playAnimation();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
