package com.yuvix.tourustan;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

//import com.mukesh.OnOtpCompletionListener;
//import com.mukesh.OtpView;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static utils.Constants.API_LINK_V2;
import static utils.Constants.USER_EMAIL;
import static utils.Constants.USER_TOKEN;
import static utils.Constants.VERIFICATION_REQUEST_CODE;

public class VerifyEmailActivity extends AppCompatActivity {

    @BindView(R.id.code_sent_alert)
    TextView mCodeSentAlert;

//    @BindView(R.id.resend_code)
//    TextView resendCode;
//
//    @BindView(R.id.otp_code)
//    OtpView resetCode;

    private String mToken;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_email);
        ButterKnife.bind(this);
        mHandler = new Handler(Looper.getMainLooper());
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);

//        resendCode.setOnClickListener(view -> verifyEmail(null, false));
//        resetCode.setOtpCompletionListener(this);

        String mUserEmail = mSharedPreferences.getString(USER_EMAIL, null);
        mCodeSentAlert.setText(String.format(getString(R.string.text_otp_code_sent_alert), mUserEmail));
    }

//    @Override
//    public void onOtpCompleted(String otp) {
//        if (otp.trim().length() == 6) {
//            verifyEmail(otp.trim(), true);
//        }
//    }


    public void verifyEmail(String code, boolean verify) {

    }

}
