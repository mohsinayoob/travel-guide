package com.yuvix.tourustan.friend;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.squareup.picasso.Picasso;
import com.yuvix.tourustan.FullScreenImage;
import com.yuvix.tourustan.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import objects.FriendCity;
import objects.Trip;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import utils.TravelmateSnackbars;

import static android.view.View.GONE;
import static com.google.android.flexbox.FlexDirection.ROW;
import static com.google.android.flexbox.JustifyContent.FLEX_START;
import static utils.Constants.API_LINK_V2;
import static utils.Constants.EXTRA_MESSAGE_FRIEND_ID;
import static utils.Constants.USER_TOKEN;
import static utils.DateUtils.getDate;
import static utils.DateUtils.rfc3339ToMills;


public class FriendsProfileActivity extends AppCompatActivity implements TravelmateSnackbars {

    @BindView(R.id.display_image)
    ImageView friendDisplayImage;
    @BindView(R.id.display_email)
    TextView friendsEmail;
    @BindView(R.id.display_name)
    TextView friendUserName;
    @BindView(R.id.display_joining_date)
    TextView friendJoiningDate;
    @BindView(R.id.display_status)
    TextView displayStatus;
    @BindView(R.id.status_icon)
    ImageView statusIcon;
    @BindView(R.id.profile_icon)
    ImageView profileIcon;
    @BindView(R.id.date_joined_icon)
    ImageView dateJoinedIcon;
    @BindView(R.id.email_icon)
    ImageView emailIcon;
    @BindView(R.id.trips_together_layout)
    RelativeLayout tripsTogetherLayout;
    @BindView(R.id.date_joined_layout)
    LinearLayout dateJoinedLayout;
    @BindView(R.id.display_mutual_trips)
    TextView mutualTripsText;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.friend_city_list_recycler)
    RecyclerView cityRecycler;

    private String mToken;
    private Handler mHandler;
    private String mFriendImageUri;
    private String mFriendName;
    private List<Trip> mTrips = new ArrayList<>();
    private RecyclerView.Adapter mMutualTripsAdapter;
    private FriendCityRecyclerAdapter mCityAdapter;
    private ArrayList<FriendCity> mFriendCityList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_profile);
        ButterKnife.bind(this);

        mFriendCityList = new ArrayList<>();
        mCityAdapter = new FriendCityRecyclerAdapter(this, mFriendCityList);
        cityRecycler.setAdapter(mCityAdapter);
        cityRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        Intent intent = getIntent();
        int mFriendId = (int) intent.getSerializableExtra(EXTRA_MESSAGE_FRIEND_ID);
        mHandler = new Handler(Looper.getMainLooper());
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);

        getFriendDetails(String.valueOf(mFriendId));
        getMutualTrips(String.valueOf(mFriendId));
        getVisitedCities(String.valueOf(mFriendId));
        Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //open friend's profile image in full screen
        friendDisplayImage.setOnClickListener(v -> {
            Intent fullScreenIntent = FullScreenImage.getStartIntent(FriendsProfileActivity.this,
                    mFriendImageUri, mFriendName);
            startActivity(fullScreenIntent);
        });
    }

    public static Intent getStartIntent(Context context, int id) {
        Intent intent = new Intent(context, FriendsProfileActivity.class);
        intent.putExtra(EXTRA_MESSAGE_FRIEND_ID, id);
        return intent;
    }

    private void getVisitedCities(final String friendId) {

    }

    private void getFriendDetails(final String friendId) {
    }

    /**
     * Fetches list of all trips user has travelled with given friend
     */
    private void getMutualTrips(String friendId) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // return back to trip activity
                finish();
                return true;
            default :
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Plays the network lost animation in the view
     */
    private void networkError() {
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

}