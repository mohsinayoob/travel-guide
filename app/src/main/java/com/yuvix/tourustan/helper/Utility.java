package com.yuvix.tourustan.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility
{
    private static Pattern pattern;
    private static Matcher matcher;
    private static final String EMAIL_PATTERN =  "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"  + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static NetworkInfo localNetworkInfo;
    private static URL url;
    private static Bitmap image;
    private static String path;
    private static Uri uri;
    private static int imageResource;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    public static Context context;
    public static String[] mThumbName = {
            "route finder",
            "my location",
            "hospital",
            "hotel",
            "mosque",
            "airport",
            "bank",
            "atm",
            "post office",
            "university",
            "school",
            "clothing store",
            "fire station",
            "police station",
            "park",
            "bakery",
            "cafe",
            "service station"
    };

    public static int getFamilyPosition(int target) {
        int position = -1;

        for (int i = 0; i < Family.length; i++) {
            if(Family[i] == target){
                position = i;
            }
        }

        return position;
    }

    public static int getPosition(String target){
        int position = -1;

        for (int i = 0; i < mThumbName.length; i++) {
            if(mThumbName[i] == target){
                position = i;
            }
        }

        return position;
    }
//    public  static int[] Student = { 0,1,2,11,16,14,9,10,4};
//    public  static int[] StudentImage = { R.drawable.p1,R.drawable.p2,R.drawable.p3,R.drawable.p12,R.drawable.p17,R.drawable.p15,R.drawable.p10,R.drawable.p11,R.drawable.p5};
//
//    public  static  int[]     Tourist= { 0,1,2,5,16,3,11,7,17 };
//    public  static  int[]     TouristImage= { R.drawable.p1,R.drawable.p2,R.drawable.p3,R.drawable.p6,R.drawable.p17,R.drawable.p4,R.drawable.p12,R.drawable.p8,R.drawable.p18 };
//
//    public  static  int[]     Business= { 0,1,2,3,6,7,17,5,4 };
//    public  static  int[]     BusinessImage= {R.drawable.p1,R.drawable.p2,R.drawable.p3,R.drawable.p4,R.drawable.p7,R.drawable.p8,R.drawable.p18,R.drawable.p6,R.drawable.p5 };
//
    public  static  int[]    Family ={ 0,1,2,3,4,14,15,16,17 };
//    public  static  int[]    FamilyImage ={ R.drawable.p1,R.drawable.p2,R.drawable.p3,R.drawable.p4,R.drawable.p5,R.drawable.p15,R.drawable.p14,R.drawable.p17,R.drawable.p18 };
//
//    public  static  int[]    Friends= { 0,1,2,4,12,13,14,15,16 };
//    public  static  int[]    FriendsImage= { R.drawable.p1,R.drawable.p2,R.drawable.p3,R.drawable.p5,R.drawable.p13,R.drawable.p14,R.drawable.p15,R.drawable.p16,R.drawable.p17 };
    public static boolean validate(String email)
    {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public static boolean isNotNull(String txt)
    {
        return txt!=null && txt.trim().length()>0 ? true: false;
    }
    public static boolean isOnline(Context context)
    {
        localNetworkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        return (localNetworkInfo != null) && (localNetworkInfo.isConnectedOrConnecting());
    }
    public static Drawable getDrawable(Context context , String imageName)
    {
        try
        {
            imageResource = context.getResources().getIdentifier(imageName, null, context.getPackageName());
            return context.getResources().getDrawable(imageResource);
        }
        catch (Exception e)
        {
            return null;
        }
    }
    public static void showToast(Context context , String msg)
    {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
    public void setSharedPreferencesData(String name , String value)
    {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
        editor.putString(name , value);
        editor.commit();
    }
    public String getSharedPreferencesData(String name , String defaultValue)
    {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return  sharedPreferences.getString(name , defaultValue);
    }
    public static int getHeight(double height)
    {
        double z = (height/100) * (context.getResources().getDisplayMetrics().heightPixels);
        return (int)z;
    }

}