package com.yuvix.tourustan.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cloudinary.api.exceptions.ApiException;
import com.dd.processbutton.FlatButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
//import com.mukesh.OnOtpCompletionListener;
//import com.mukesh.OtpView;
import com.yuvix.tourustan.MainActivity;
import com.yuvix.tourustan.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

import objects.User;
import utils.TravelmateSnackbars;

import static utils.Constants.GOOGLE_SIGN_IN;
import static utils.Constants.USER_EMAIL;
import static utils.Constants.USER_TOKEN;


/**
 * Initiates login
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginView,
        TravelmateSnackbars {

    private static final int RC_SIGN_IN = 100;
    private final LoginPresenter mLoginPresenter = new LoginPresenter();

    @BindView(R.id.signup)
    TextView signup;
    @BindView(R.id.login)
    TextView login;
    @BindView(R.id.back_to_login)
    TextView mBackToLogin;

    @BindView(R.id.signup_layout)
    LinearLayout sig;
    @BindView(R.id.loginlayout)
    LinearLayout log;
    @BindView(R.id.forgot_password_layout)
    LinearLayout mForgotPasswordLayout;
    @BindView(R.id.reset_code_layout)
    LinearLayout mResetCodeLayout;
    @BindView(R.id.new_password_layout)
    LinearLayout mNewPasswordLayout;

    @BindView(R.id.input_username_login)
    EditText username_login;
    @BindView(R.id.input_pass_login)
    EditText pass_login;
    @BindView(R.id.input_username_signup)
    EditText username_signup;
    @BindView(R.id.input_pass_signup)
    EditText pass_signup;
    @BindView(R.id.input_confirm_pass_signup)
    EditText confirm_pass_signup;
    @BindView(R.id.input_firstname_signup)
    EditText firstName;
    @BindView(R.id.input_lastname_signup)
    EditText lastName;
    @BindView(R.id.input_email_forgot_password)
    EditText mEmailForgotPassword;
    @BindView(R.id.input_pass_reset)
    EditText mNewPasswordReset;
    @BindView(R.id.input_confirm_pass_reset)
    EditText mConfirmNewPasswordReset;

    @BindView(R.id.ok_login)
    FlatButton ok_login;
    @BindView(R.id.ok_signup)
    FlatButton ok_signup;
    @BindView(R.id.ok_submit_pass_reset)
    FlatButton mOkSubmitReset;
    @BindView(R.id.ok_confirm_pass_reset)
    FlatButton mOkConfirmReset;

    @BindView(R.id.forgot_password)
    TextView mForgotPasswordText;
    @BindView(R.id.code_sent_alert)
    TextView mCodeSentAlert;
    @BindView(R.id.resend_code)
    TextView mResendCodeText;

//    @BindView(R.id.reset_code)
//    OtpView mResetCode;

    @BindView(R.id.sign_in_button)
    SignInButton signInButton;

    private SharedPreferences mSharedPreferences;
    private MaterialDialog mDialog;
    private Handler mHandler;
    private String mOtpCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.black));
        }

        mLoginPresenter.bind(this);
        ButterKnife.bind(this);

        // Initialization
        mHandler = new Handler(Looper.getMainLooper());
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mLoginPresenter.setPreferences(mSharedPreferences);
        // Get runtime permissions for Android M
        getRunTimePermissions();

        signup.setOnClickListener(this);
        login.setOnClickListener(this);
        ok_login.setOnClickListener(this);
        ok_signup.setOnClickListener(this);
        signInButton.setOnClickListener(this);

        //listeners for handling 'forgot password' flow
        mForgotPasswordText.setOnClickListener(this);
        mBackToLogin.setOnClickListener(this);
        mResendCodeText.setOnClickListener(this);
        mOkSubmitReset.setOnClickListener(this);
        mOkConfirmReset.setOnClickListener(this);

        //listener for otp view to accept password reset code
//        mResetCode.setOtpCompletionListener(this);
    }

    @Override
    protected void onDestroy() {
        mLoginPresenter.unbind();
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            // Open signup
            case R.id.signup:
                signup.setVisibility(View.GONE);
                mForgotPasswordText.setVisibility(View.GONE);
                mBackToLogin.setVisibility(View.GONE);
                login.setVisibility(View.VISIBLE);
                mLoginPresenter.signUp();
                break;
            // Open login
            case R.id.login:
                signup.setVisibility(View.VISIBLE);
                mForgotPasswordText.setVisibility(View.VISIBLE);
                mBackToLogin.setVisibility(View.GONE);
                login.setVisibility(View.GONE);
                mLoginPresenter.login();
                break;
            // Call login
            case R.id.ok_login:
                String usernameString = username_login.getText().toString();
                String passString = pass_login.getText().toString();
                if(usernameString.contains(".")){
                    username_login.setError("Invalid username");
                }else {
                    mLoginPresenter.ok_login(usernameString, passString, mHandler);
                }
                break;
            // Call signup
            case R.id.ok_signup:
                usernameString = username_signup.getText().toString();
                passString = pass_signup.getText().toString();
                String confirmPassString = confirm_pass_signup.getText().toString();
                String firstname = firstName.getText().toString();
                String lastname = lastName.getText().toString();
//                if (validateEmail(emailString)) {
                    if (validatePassword(passString)) {
                        if (passString.equals(confirmPassString)) {
                            mLoginPresenter.ok_signUp(firstname, lastname, usernameString, passString, mHandler);
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(findViewById(android.R.id.content),
                                          R.string.passwords_check, Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                    }
//                }
                break;
                // Open forgot password
            case R.id.forgot_password:
                login.setVisibility(View.GONE);
                signup.setVisibility(View.GONE);
                mForgotPasswordText.setVisibility(View.GONE);
                mBackToLogin.setVisibility(View.VISIBLE);
                mLoginPresenter.forgotPassword();
                break;
                // Call submit password reset request
            case R.id.ok_submit_pass_reset:
                usernameString = mEmailForgotPassword.getText().toString();
                if (!usernameString.equals("") && !usernameString.contains(".")) {
                    mBackToLogin.setVisibility(View.GONE);
                    mLoginPresenter.ok_password_reset_request(usernameString, mHandler);
                }
                break;
                // Open login
            case R.id.back_to_login:
                signup.setVisibility(View.VISIBLE);
                mForgotPasswordText.setVisibility(View.VISIBLE);
                mBackToLogin.setVisibility(View.GONE);
                login.setVisibility(View.GONE);
                mLoginPresenter.login();
                break;
                // Call resend reset code request
            case R.id.resend_code:
                usernameString = mEmailForgotPassword.getText().toString();
                mLoginPresenter.resendResetCode(usernameString, mHandler);
                break;
                // Call confirm reset request
            case R.id.ok_confirm_pass_reset:
                usernameString = mEmailForgotPassword.getText().toString();
                mLoginPresenter.ok_password_reset_confirm(usernameString);
                break;
            case R.id.sign_in_button:
                signIn();
                break;
        }
    }

    private void signIn() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        showLoadingDialog();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();

        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLoginPresenter.bind(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        dismissLoadingDialog();
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            rememberUserInfo(account.getIdToken(), account.getEmail());
            mSharedPreferences.edit().putBoolean(GOOGLE_SIGN_IN, true).apply();
            DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
            databaseRef.child("Users").child(account.getEmail().replace(".", "")).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        dismissLoadingDialog();
                        showMessage("Username Already Registered");
                    } else {
                        databaseRef.child("Users").child(account.getEmail().replace(".", "")).setValue(
                                new User(account.getDisplayName(), account.getFamilyName(), null, account.getGivenName()), (databaseError, databaseReference) -> {
                                    dismissLoadingDialog();
                                    if (databaseError != null) {
                                        showMessage("Signup Unsuccessful");
                                    } else {
                                        startMainActivity();
                                        showMessage("Signin Successful");
                                    }
                                });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(getClass().getSimpleName(), databaseError.toString());
                    showMessage("Signup Unsuccessful");
                    dismissLoadingDialog();
                }
            });

            startMainActivity();
            // Signed in successfully, show authenticated UI.
        } catch (Exception e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(getClass().getSimpleName(), "signInResult:failed code=" + e.toString());
        }
    }



    @Override
    public void rememberUserInfo(String token, String email) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_TOKEN, token);
        editor.putString(USER_EMAIL, email);
        editor.apply();
    }

    @Override
    public void startMainActivity() {
        Intent i = MainActivity.getStartIntent(this);
        startActivity(i);
        finish();
    }

    @Override
    public void showError() {
        TravelmateSnackbars.createSnackBar(findViewById(R.id.login_activity),
                R.string.toast_invalid_username_or_password, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoadingDialog() {
        mDialog = new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .content(R.string.progress_wait)
                .progress(true, 0)
                .show();
    }

    @Override
    public void dismissLoadingDialog() {
        mDialog.dismiss();
    }

    @Override
    public void getRunTimePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(LoginActivity.this,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.WRITE_CONTACTS,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.WAKE_LOCK,
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.VIBRATE,

                }, 0);
            }
        }
    }

    @Override
    public void openSignUp() {
        log.setVisibility(View.GONE);
        mForgotPasswordLayout.setVisibility(View.GONE);
        sig.setVisibility(View.VISIBLE);
    }

    @Override
    public void openLogin() {
        showMessage(getString(R.string.text_password_updated_alert));
        mForgotPasswordLayout.setVisibility(View.GONE);
        mNewPasswordLayout.setVisibility(View.GONE);
        sig.setVisibility(View.GONE);
        log.setVisibility(View.VISIBLE);
        mForgotPasswordText.setVisibility(View.VISIBLE);
        mEmailForgotPassword.setText("");
    }

    /**
     * displays input fields to get user's email when he/she requests for a password reset
     */
    @Override
    public void forgotPassword() {
        log.setVisibility(View.GONE);
        sig.setVisibility(View.GONE);
        mForgotPasswordLayout.setVisibility(View.VISIBLE);
    }

    /**
     * displays pin view for a user to enter 4-digit code that verifies his/her request for password reset
     * @param email user's e-mail address to which a newly generated 4-digit code will be sent
     */
    @Override
    public void openResetPin(String email) {
        mForgotPasswordLayout.setVisibility(View.GONE);
//        mResetCodeLayout.setVisibility(View.VISIBLE);
        mCodeSentAlert.setText(String.format(getString(R.string.text_code_sent_alert), email));
        mResendCodeText.setVisibility(View.VISIBLE);
    }

    /**
     * displays a confirmation alert that a reset code has been resent upon request
     */
    @Override
    public void resendResetCode() {
        showMessage(getString(R.string.text_code_resent_alert));
    }

    /**
     * displays fields to input new password and confirm the new password
     */
    @Override
    public void newPasswordInput() {
//        mResetCodeLayout.setVisibility(View.GONE);
        mResendCodeText.setVisibility(View.GONE);
        mNewPasswordLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void confirmPasswordReset(String email) {

    }

    public void setLoginEmail(String email) {
        username_login.setText(email);
    }

    public void showMessage(String message) {
        Snackbar snackbar = Snackbar
                .make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    /**
     * Validates the given email, checks if given email proper format as standard email string
     * @param email email string to be validate
     * @return Boolean returns true if email format is correct, otherwise false
     */
    public boolean validateEmail(String email) {
        Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(email);
        if (!email.equals("") && matcher.matches()) {
            return true;
        } else {
            Snackbar snackbar = Snackbar
                    .make(findViewById(android.R.id.content), R.string.invalid_email, Snackbar.LENGTH_LONG);
            snackbar.show();
            return false;
        }
    }

    /**
     * Validates the given password, checks if given password proper format as standard password string
     * @param passString password string to be validate
     * @return Boolean returns true if email format is correct, otherwise false
     */
    public boolean validatePassword(String passString) {
        if (passString.length() >= 8) {
            Pattern pattern;
            Matcher matcher;
            final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[_@#$%^?&+=!])(?=\\S+$).{4,}$";
            pattern = Pattern.compile(PASSWORD_PATTERN);
            matcher = pattern.matcher(passString);

            if (matcher.matches()) {
                return true;
            } else {
                Snackbar snackbar = Snackbar
                        .make(findViewById(android.R.id.content), R.string.invalid_password, Snackbar.LENGTH_LONG);
                snackbar.show();
                return false;
            }
        } else {
            Snackbar snackbar = Snackbar
                    .make(findViewById(android.R.id.content), R.string.password_length, Snackbar.LENGTH_LONG);
            snackbar.show();
            return false;
        }
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    /**
     * handles validating password reset confirmation code
<<<<<<< HEAD
=======
     *
>>>>>>> a463f244c51605faabe2ce746a12b65144a4d005
     */
//    @Override
//    public void onOtpCompleted(String otp) {
//        mOtpCode = otp;
//        mLoginPresenter.newPasswordInput();
//    }

    @Override
    public void showDialog(String str){
        android.support.v7.app.AlertDialog alertDialog = new
                android.support.v7.app.AlertDialog.Builder(this).create();
        alertDialog.setTitle("Password");
        alertDialog.setMessage("Your password is:" + str);
        alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.compass_dialog_confirm),
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }
}