package com.yuvix.tourustan.login;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.yuvix.tourustan.MainActivity;
import com.yuvix.tourustan.utilities.CompassActivity;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Objects;

import objects.User;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static utils.Constants.API_LINK_V2;
import static utils.Constants.GOOGLE_SIGN_IN;

/**
 * Created by el on 5/4/17.
 */

class LoginPresenter {
    private LoginView mView;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseRef = database.getReference();
    private SharedPreferences mPreference;

    public void setPreferences(SharedPreferences mPreference){this.mPreference = mPreference;}
    public void bind(LoginView view) {
        this.mView = view;
    }

    public void unbind() {
        mView = null;
    }

    public void signUp() {
        mView.openSignUp();
    }


    /**
     * Calls Signup API
     *
     * @param firstname user's first name
     * @param lastname  user's last name
     * @param username     user's email id
     * @param pass      password user entered
     */
    public void ok_signUp(final String firstname, final String lastname, final String username,
                          final String pass, Handler handler) {

        mView.showLoadingDialog();

        if (!username.contains(".")){

            databaseRef.child("Users").child(username).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        mView.dismissLoadingDialog();
                        mView.showMessage("Username Already Registered");
                    } else {
                        databaseRef.child("Users").child(username).setValue(
                                new User(firstname, lastname, pass, username, generateToken()), (databaseError, databaseReference) -> {
                                    mView.dismissLoadingDialog();
                                    if (databaseError != null) {
                                        mView.showMessage("Signup Unsuccessful");
                                    } else {
                                        mView.openLogin();
                                        mView.setLoginEmail(username);
                                        mView.showMessage("signup succeeded! please login");
                                    }
                                });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(getClass().getSimpleName(), databaseError.toString());
                    mView.showMessage("Signup Unsuccessful");
                    mView.dismissLoadingDialog();
                }
            });
        }else{
            mView.dismissLoadingDialog();
            mView.showMessage("Username cannot contain \".\"");
        }
    }

    public void login() {
        mView.openLogin();
    }

    /**
     * Calls Login API and checks for validity of credentials
     * If yes, transfer to MainActivity
     *
     * @param username user's email id
     * @param pass  password user entered
     */
    public void ok_login(final String username, String pass, final Handler mhandler) {
        mView.showLoadingDialog();

        databaseRef.child("Users").child(username).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(mView!=null) {
                    mView.dismissLoadingDialog();

                    if (dataSnapshot.getValue() != null) {
                        String password = dataSnapshot.child("mPassword").getValue().toString();
                        String token = dataSnapshot.child("mtoken").getValue().toString();
                        if (password.equals(pass)) {
                            mView.showMessage("Login Successfull");
                            mView.rememberUserInfo(token, username);
                            mPreference.edit().putBoolean(GOOGLE_SIGN_IN, false).apply();
                            mView.startMainActivity();
                        } else {
                            mView.showMessage("Username or Password Invalid");
                        }
                    } else {
                        mView.showMessage("Username or Password Invalid");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                if(mView!=null) {
                    mView.dismissLoadingDialog();
                    mView.showMessage(databaseError.toString());
                }
            }
        });
    }

    public void forgotPassword() {
        mView.forgotPassword();
    }

    /**
     * called when a user submits his/her e-mail address for which the password needs to be reset
     *
     * @param username user's email address
     */
    public void ok_password_reset_request(String username, Handler mHandler) {
        mView.showLoadingDialog();
        databaseRef.child("Users").child(username).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mView.dismissLoadingDialog();
                if (dataSnapshot.exists()) {
                    String pass = (String) dataSnapshot.child("mPassword").getValue();
                    mView.showDialog(pass);
                } else {
                    mView.showMessage("Username not registered");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(getClass().getSimpleName(), databaseError.toString());
                mView.showMessage("Signup Unsuccessful");
                mView.dismissLoadingDialog();
            }
        });
    }

    /**
     * triggers a request to resend a 4-digit code for validation password reset request
     *
     * @param email user's email address to where the code has to be sent
     */
    public void resendResetCode(String email, Handler mHandler) {
        ok_password_reset_request(email, mHandler);

        mView.resendResetCode();
    }

    public void newPasswordInput() {
        mView.newPasswordInput();
    }

    public void ok_password_reset_confirm(String email) {
        mView.confirmPasswordReset(email);
    }

    private String generateToken() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        return bytes.toString();
    }
}