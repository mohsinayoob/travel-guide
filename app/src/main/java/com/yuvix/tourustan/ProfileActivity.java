package com.yuvix.tourustan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.airbnb.lottie.LottieAnimationView;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.yuvix.tourustan.login.LoginActivity;
import com.yuvix.tourustan.utilities.ShareContactActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import objects.City;
import objects.User;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import utils.CircleImageView;
import utils.TravelmateSnackbars;

import static android.view.View.GONE;
import static com.google.android.flexbox.FlexDirection.ROW;
import static com.google.android.flexbox.JustifyContent.FLEX_START;
import static utils.Constants.API_LINK_V2;
import static utils.Constants.CLOUDINARY_API_KEY;
import static utils.Constants.CLOUDINARY_API_SECRET;
import static utils.Constants.CLOUDINARY_CLOUD_NAME;
import static utils.Constants.GOOGLE_SIGN_IN;
import static utils.Constants.OTHER_USER_ID;
import static utils.Constants.SHARE_PROFILE_URI;
import static utils.Constants.SHARE_PROFILE_USER_ID_QUERY;
import static utils.Constants.USER_DATE_JOINED;
import static utils.Constants.USER_EMAIL;
import static utils.Constants.USER_ID;
import static utils.Constants.USER_IMAGE;
import static utils.Constants.USER_NAME;
import static utils.Constants.USER_STATUS;
import static utils.Constants.USER_TOKEN;
import static utils.Constants.VERIFICATION_REQUEST_CODE;
import static utils.DateUtils.getDate;
import static utils.DateUtils.rfc3339ToMills;

public class ProfileActivity extends AppCompatActivity implements TravelmateSnackbars {
    @BindView(R.id.horizontalProgressBar)
    ProgressBar horizontalProgressBar;
    @BindView(R.id.display_image)
    CircleImageView displayImage;
    @BindView(R.id.change_image)
    CircleImageView changeImage;
    @BindView(R.id.display_name)
    EditText displayName;
    @BindView(R.id.display_email)
    TextView emailId;
    @BindView(R.id.is_email_verified)
    ImageView isVerified;
    @BindView(R.id.display_joining_date)
    TextView joiningDate;
    @BindView(R.id.display_status)
    EditText displayStatus;
    @BindView(R.id.ib_edit_display_name)
    ImageButton editDisplayName;
    @BindView(R.id.ib_edit_display_status)
    ImageButton editDisplayStatus;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.status_progress_bar)
    ProgressBar statusProgressBar;
    @BindView(R.id.name_progress_bar)
    ProgressBar nameProgressBar;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.status_character_count)
    TextView characterCount;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.citie_travelled_text)
    TextView citiesTravelledHeading;

    private String mToken;
    private Handler mHandler;
    private String mUserStatus;
    private List<City> mCities = new ArrayList<>();
    // Flag for checking the current drawable of the ImageButton
    private boolean mFlagForDrawable = true;
    private SharedPreferences mSharedPreferences;
    private Menu mOptionsMenu;
    //request code for picked image
    private static final int RESULT_PICK_IMAGE = 1;
    //request code for cropped image
    private static final int RESULT_CROP_IMAGE = 2;
    private static final String LOG_TAG = ProfileActivity.class.getSimpleName();
    private String mProfileImageUrl;
    private CitiesTravelledAdapter mCitiesAdapter;
    private MaterialDialog mDialog;
    private boolean mIsVerified;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        animationView.setVisibility(View.GONE);
        mHandler = new Handler(Looper.getMainLooper());
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);

        Intent intent = getIntent();
        String id = intent.getStringExtra(OTHER_USER_ID);
        getTravelledCities();
        getUserDetails(id);
        Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (id == null) {
            fillProfileInfo(mSharedPreferences.getString(USER_NAME, null),
                    mSharedPreferences.getString(USER_EMAIL, null),
                    mSharedPreferences.getString(USER_IMAGE, null),
                    mSharedPreferences.getString(USER_DATE_JOINED, null),
                    mSharedPreferences.getString(USER_STATUS, getString(R.string.default_status)));

        } else {
            editDisplayName.setVisibility(View.INVISIBLE);
            updateOptionsMenu();
        }

        isVerified.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
            if (!mIsVerified) {
                builder.setTitle(R.string.email_not_verified);
                builder.setPositiveButton(R.string.verify_now, (dialogInterface, i) -> {
                    sendVerificationEmail();
                });
                builder.setNegativeButton(R.string.later, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                });
            } else {
                builder.setTitle(R.string.text_email_verified);
                builder.setPositiveButton(R.string.positive_button, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                });
            }
            builder.create().show();

        });

        editDisplayName.setOnClickListener(v -> {
            if (mFlagForDrawable) {
                mFlagForDrawable = false;
                editDisplayName.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_black_24dp));
                displayName.setFocusableInTouchMode(true);
                displayName.setCursorVisible(true);
                displayName.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                Objects.requireNonNull(imm).showSoftInput(displayName, InputMethodManager.SHOW_IMPLICIT);
            } else {
                mFlagForDrawable = true;
                editDisplayName.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_black_24dp));
                displayName.setFocusableInTouchMode(false);
                displayName.setCursorVisible(false);
                setUserDetails();
            }
        });

        editDisplayStatus.setOnClickListener(v -> {
            if (mFlagForDrawable) {
                mFlagForDrawable = false;
                editDisplayStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_black_24dp));
                displayStatus.setFocusableInTouchMode(true);
                displayStatus.setCursorVisible(true);
                displayStatus.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                Objects.requireNonNull(imm).showSoftInput(displayStatus, InputMethodManager.SHOW_IMPLICIT);
                characterCount.setVisibility(View.VISIBLE);
            } else {
                mFlagForDrawable = true;
                editDisplayStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_black_24dp));
                displayStatus.setFocusableInTouchMode(false);
                displayStatus.setCursorVisible(false);
                characterCount.setVisibility(View.GONE);
            }
        });
        displayStatus.addTextChangedListener(mCountCharacters);
        changeImage.setOnClickListener(v -> {
            Intent galleryIntent = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            Intent removeIntent = new Intent(Intent.ACTION_DELETE);
            Intent chooserIntent = Intent.createChooser(removeIntent, getString(R.string.choose_an_option));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{galleryIntent});
            startActivityForResult(chooserIntent, RESULT_PICK_IMAGE);
        });

        //open profile image when clicked on it
        displayImage.setOnClickListener(v -> {
            String imageUri = mSharedPreferences.getString(USER_IMAGE, null);
            String fullname = mSharedPreferences.getString(USER_NAME, null);
            Intent fullScreenIntent = FullScreenImage.getStartIntent(ProfileActivity.this,
                    imageUri, fullname);
            startActivity(fullScreenIntent);
        });
    }


    public void sendVerificationEmail() {

    }

    private final TextWatcher mCountCharacters = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            characterCount.setText(String.valueOf(s.length()) + getString(R.string.status_character_limit));
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == VERIFICATION_REQUEST_CODE) {
            recreate();
        }

        if (data == null)
            return;

        //After user has picked the image
        if (requestCode == RESULT_PICK_IMAGE && data.hasExtra("remove_image")) {
            deleteProfilePicture();
        } else if (requestCode == RESULT_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            //startCropIntent(selectedImage);
            CropImage.activity(selectedImage).start(this);
        }
        //After user has cropped the image
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri croppedImage = result.getUri();
                Picasso.with(this).load(croppedImage).into(displayImage);
                mSharedPreferences.edit().putString(USER_IMAGE, croppedImage.toString()).apply();
                TravelmateSnackbars.createSnackBar(findViewById(R.id.layout), R.string.profile_picture_updated,
                        Snackbar.LENGTH_SHORT).show();
                getUrlFromCloudinary(croppedImage);
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View view = getCurrentFocus();
            if (view instanceof EditText) {
                Rect outRect = new Rect();
                view.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    view.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    Objects.requireNonNull(imm).hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            case R.id.action_sign_out:
                signOut();
                return true;
            case R.id.action_share_profile:
                shareProfile();
                return true;
            case R.id.action_qrcode_scan:
                Intent intent;
                intent = ShareContactActivity.getStartIntent(ProfileActivity.this);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void signOut() {
        //set AlertDialog before signout
        ContextThemeWrapper crt = new ContextThemeWrapper(this, R.style.AlertDialog);
        AlertDialog.Builder builder = new AlertDialog.Builder(crt);
        builder.setMessage(R.string.signout_message)
                .setPositiveButton(R.string.positive_button,
                        (dialog, which) -> {
                            mSharedPreferences.edit().putBoolean(GOOGLE_SIGN_IN, false);

                            mSharedPreferences
                                    .edit()
                                    .putString(USER_EMAIL, null)
                                    .putString(USER_TOKEN, null)
                                    .apply();
                            Intent i = LoginActivity.getStartIntent(ProfileActivity.this);
                            startActivity(i);
                            finish();
                        })
                .setNegativeButton(android.R.string.cancel,
                        (dialog, which) -> {

                        });
        builder.create().show();
    }

    private void deleteProfilePicture() {

    }


    private void getUserDetails(final String userId) {

    }

    private void setUserDetails() {

    }


    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ProfileActivity.class);
        return intent;
    }

    public static Intent getStartIntent(Context context, String userId) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra(OTHER_USER_ID, userId);
        return intent;
    }

    private void fillProfileInfo(String fullName, String email, String imageURL,
                                 String dateJoined, String status) {
        displayName.setText(fullName);
        emailId.setText(email);
        joiningDate.setText(String.format(getString(R.string.text_joining_date), dateJoined));
        Picasso.with(ProfileActivity.this).load(imageURL).placeholder(R.drawable.default_user_icon)
                .error(R.drawable.default_user_icon).into(displayImage);
        setTitle(fullName);
        if (status.equals("null"))
            status = getString(R.string.default_status);
        displayStatus.setText(status);
    }

    /**
     * Method to get URL for image using Cloudinary
     *
     * @param croppedImage Uri of cropped image
     **/
    private void getUrlFromCloudinary(Uri croppedImage) {
        Map config = new HashMap();
        config.put("cloud_name", CLOUDINARY_CLOUD_NAME);
        config.put("api_key", CLOUDINARY_API_KEY);
        config.put("api_secret", CLOUDINARY_API_SECRET);
        MediaManager.init(this, config);

        mHandler.post(() -> MediaManager.get().upload(croppedImage).callback(new UploadCallback() {
            @Override
            public void onStart(String requestId) {

            }

            @Override
            public void onSuccess(String requestId, Map resultData) {
                mProfileImageUrl = resultData.get("url").toString();
                sendURLtoServer(mProfileImageUrl);
            }

            @Override
            public void onError(String requestId, ErrorInfo error) {
                networkError();
                Log.e(LOG_TAG, "error uploading to Cloudinary");
            }

            @Override
            public void onReschedule(String requestId, ErrorInfo error) {
                Log.e(LOG_TAG, error.toString());
            }

            @Override
            public void onProgress(String requestId, long bytes, long totalBytes) {

            }
        }).dispatch());
    }

    /**
     * Method for sending URL to server
     *
     * @param imageUrl - Url of image obtained from
     *                 Cloudinary cloud(passed as string)
     */
    private void sendURLtoServer(String imageUrl) {

    }


    /**
     * Fetches list of all cities user has travelled
     */
    private void getTravelledCities() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        mOptionsMenu = menu;
        return true;
    }

    private void updateOptionsMenu() {
        if (mOptionsMenu != null) {
            MenuItem item = mOptionsMenu.findItem(R.id.action_share_profile);
            item.setVisible(false);
            MenuItem qrItem = mOptionsMenu.findItem(R.id.action_qrcode_scan);
            qrItem.setVisible(false);
        }
    }

    /**
     * Plays the network lost animation in the view
     */
    private void networkError() {
        layout.setVisibility(View.INVISIBLE);
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

    private void shareProfile() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        Uri profileURI = Uri.parse(SHARE_PROFILE_URI)
                .buildUpon()
                .appendQueryParameter(SHARE_PROFILE_USER_ID_QUERY, mSharedPreferences.getString(USER_ID, null))
                .build();

        Log.v("profile url", profileURI + "");

        intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_profile_text) + " " + profileURI);
        try {
            startActivity(Intent.createChooser(intent, getString(R.string.share_chooser)));
        } catch (android.content.ActivityNotFoundException ex) {
            TravelmateSnackbars.createSnackBar(findViewById(R.id.layout), R.string.snackbar_no_share_app,
                    Snackbar.LENGTH_LONG).show();
        }
    }
}