package com.yuvix.tourustan;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.yuvix.tourustan.helper.Utility;


public class HomeMapScreenActivity extends AppCompatActivity implements LocationFragment.RemoveLocationEventListener,
        View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    private static final String TAG = "HomeMapScreenActivity";

    private static Activity activity;

    public SharedPreferences sharedPreferences;

    public SharedPreferences.Editor edit;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Location mLastLocation;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 1000; // 1 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 1; // 1 meters
    public View progressBar;
    public int Position;
    String latlng;
    public String Lat = null;
    public String Long = null;
    public String Url;
    public WebView webview;
    private boolean check = false;

    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home_map_screen);
            progressBar = findViewById(R.id.progressBar);
            context = this;
            activity = this;
            Position = getIntent().getIntExtra("position", -1);
            latlng = getIntent().getStringExtra("latlng");
            if (!Utility.isOnline(context)) {
                Toast.makeText(context, "No Internet", Toast.LENGTH_SHORT).show();
            } else {
                progressBar.setVisibility(View.VISIBLE);
                Toast.makeText(context, "Wait", Toast.LENGTH_SHORT).show();
                // First we need to check availability of play services
                if (checkPlayServices()) {
                    // Building the GoogleApi client
                    buildGoogleApiClient();
                    mGoogleApiClient.connect();
                }

                webview = (WebView) findViewById(R.id.webView1);
                webview.setWebViewClient(new WebViewClient());
                webview.getSettings().setJavaScriptEnabled(true);
            }
        } catch (Exception e) {
        }
    }

    public Context context;

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
//        return true;
//    }

    //    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.saveRouteButton) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
    @Override
    public void onRemoveLocationEvent(String strLocationFragmentId) {

    }

    @Override
    public void onConnected(Bundle bundle) {
// Once connected with google api, get the location

        if (checkPlayServices()) {
            showMap();
            createLocationRequest();
        }

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
// Assign the new location
        mLastLocation = location;

        //Toast.makeText(getApplicationContext(), "Location changed!",Toast.LENGTH_SHORT).show();

        // Displaying the new location on UI
        storeLocation();

        // get lat long from sharedPreferences and call web sevicer here
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //String lat = sharedPreferences.getString("latitude");
        //String log =  sharedPreferences.getString("longitude");
        // call web service here
    }

    /**
     * Method to display the location on UI
     * */
    private void storeLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();
            Lat = latitude + "";
            Long = longitude + "";
            if (!check) {
                check = true;
                showMap();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        check = false;
    }

    private void showMap() {
        if(latlng != null) {
            String[] arr = latlng.split(",");
            Lat = arr[0];
            Long = arr[1];
        }else{
            if(Lat != null && Long != null) {
                storeLocation();
            }else{
                Toast.makeText(activity, "Problem in finding location", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (Position == -1) {
            Toast.makeText(context, "Nothing To Show", Toast.LENGTH_SHORT).show();
        } else if (Position == 0) {
            //Url = "https://www.google.com/maps/@"+Lat+","+Long+",13z";
            Url = "http://maps.google.com/maps?q=loc:" + Lat + "," + Long + "&z=13";
            //Url = "http://maps.google.com/maps?q=loc:"+Lat+","+Long+"&z=13";
            webview.loadUrl(Url);
        } else if (Position == 1) {
            //Url = "https://www.google.com/maps/@"+Lat+","+Long+",13z";
            Url = "http://maps.google.com/maps?q=loc:" + Lat + "," + Long + "&z=13";
            webview.loadUrl(Url);
        } else {
            Url = "https://www.google.com/maps/search/" + Utility.mThumbName[Position] + "/@" + Lat + "," + Long + ",8z";
            webview.loadUrl(Url);
        }

        // Create a Handler instance on the main thread
        final Handler handler = new Handler();

        // Create and start a new Thread
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(8000);
                } catch (Exception e) {
                } // Just catch the InterruptedException

                // Now we use the Handler to post back to the main thread
                handler.post(new Runnable() {
                    public void run() {
                        // Set the View's visibility back on the main UI Thread
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }).start();
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     * */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    @Override
    public void onClick(View v) {

    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        try{
            int resultCode = GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.SUCCESS) {
                if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                    GooglePlayServicesUtil.getErrorDialog(resultCode, HomeMapScreenActivity.activity,
                            PLAY_SERVICES_RESOLUTION_REQUEST).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "This device is not supported.", Toast.LENGTH_LONG)
                            .show();
                    //this.stopSelf();
                }
                return false;
            }
            return true;}
        catch (Exception e){return false;}
    }
}
