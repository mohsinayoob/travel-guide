package com.yuvix.tourustan.destinations.funfacts;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import objects.FunFact;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static objects.FunFact.createFunFactList;
import static utils.Constants.API_LINK_V2;

/**
 * Created by niranjanb on 14/06/17.
 */

class FunFactsPresenter {
    private final FunFactsView mFunFactsView;
    private ArrayList<String> mFactsText;
    private ArrayList<String> mFactsSourceText;
    private ArrayList<String> mFactsSourceURL;

    private ArrayList<String> mImages;
    FunFactsPresenter(FunFactsView funFactsView) {
        mFunFactsView = funFactsView;
    }

    public void initPresenter(String id, String token) {
        mFunFactsView.showProgressDialog();
        mFactsText = new ArrayList<>();
        mFactsSourceText = new ArrayList<>();
        mFactsSourceURL = new ArrayList<>();
        mImages = new ArrayList<>();
        getCityFacts(id, token);
    }

    // Fetch fun facts about city
    private void getCityFacts(final String id, final String token) {


    }


    // Fetch images of a city
    private void getCityImages(String id, String token) {

    }
}
