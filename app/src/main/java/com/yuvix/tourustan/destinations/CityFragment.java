package com.yuvix.tourustan.destinations;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.takusemba.spotlight.OnSpotlightStateChangedListener;
import com.takusemba.spotlight.OnTargetStateChangedListener;
import com.takusemba.spotlight.Spotlight;
import com.takusemba.spotlight.shape.Circle;
import com.takusemba.spotlight.target.CustomTarget;
import com.yuvix.tourustan.HomeMapScreenActivity;
import com.yuvix.tourustan.R;
import com.yuvix.tourustan.destinations.description.FinalCityInfoActivity;
import com.yuvix.tourustan.helper.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import adapters.PlaceAutocompleteAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import database.AppDataBase;
import flipviewpager.utils.FlipSettings;


import objects.AutocompletePlace;
import objects.City;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import utils.TravelmateSnackbars;

import static utils.Constants.API_LINK_V2;
import static utils.Constants.LAST_CACHE_TIME;
import static utils.Constants.SPOTLIGHT_SHOW_COUNT;
import static utils.Constants.USER_TOKEN;

public class CityFragment extends Fragment implements TravelmateSnackbars, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "CityFragment";

    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.cities_list)
    ListView lv;
    public static FrameLayout searchLayout;


    public static AutoCompleteTextView mMaterialSearchView;
    private final int[] mColors = {R.color.sienna, R.color.saffron, R.color.green, R.color.pink,
            R.color.orange, R.color.blue, R.color.grey, R.color.yellow, R.color.purple, R.color.peach};

    private String mNameyet;
    private Activity mActivity;
    private Handler mHandler;
    private String mToken;

    private int mSpotlightShownCount;
    private SharedPreferences mSharedPreferences;

    private AppDataBase mDatabase;
    private SimpleDateFormat mFormat;

    private CityAdapter mCityAdapter;
    private FlipSettings mSettings = new FlipSettings.Builder().defaultPage().build();
    private ArrayList<City> mCities = new ArrayList<>();
    private View mSpotView;
    private List<String> mInterests;

    private GoogleApiClient mGoogleApiClient;
    PlaceAutocompleteAdapter mAdapter;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(
            new LatLng(-40, -168), new LatLng(71, 136));
    private HandlerThread mHandlerThread;
    private Handler mThreadHandler;
    FrameLayout layout;
    public CityFragment() {
    }

    public static CityFragment newInstance() {
        CityFragment fragment = new CityFragment();
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_citylist, container, false);

        ButterKnife.bind(this, view);

        // Hide keyboard
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).hideSoftInputFromWindow(mActivity.getWindow().getDecorView().getWindowToken(), 0);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);
        mSpotlightShownCount = mSharedPreferences.getInt(SPOTLIGHT_SHOW_COUNT, 0);
        layout = view.findViewById(R.id.search_layout);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        mToken = sharedPreferences.getString(USER_TOKEN, null);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        mFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");

        mHandler = new Handler(Looper.getMainLooper());

        // make an target
        mSpotView = inflater.inflate(R.layout.spotlight_target, null);
        mAdapter = new PlaceAutocompleteAdapter(getActivity(), R.layout.autocomplete_list_item);
        mMaterialSearchView = view.findViewById(R.id.search_view);
        mMaterialSearchView.setAdapter(mAdapter);

        searchLayout = view.findViewById(R.id.search_layout);

        mMaterialSearchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get data associated with the specified position
                // in the list (AdapterView)
                String description = mAdapter.resultList.get(position).getName();

                mThreadHandler.removeCallbacksAndMessages(null);

                // Now add a new one
                mThreadHandler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // Background thread
                        String test = mAdapter.mPlaceAPI.LatLong(mAdapter.resultList.get(position).getPlace_id());

                        if(!test.equals("")) {
                            String [] latlng = test.split(",");

                            Intent intent = new Intent(getActivity(), HomeMapScreenActivity.class);
                            if (Utility.getPosition("park") != -1) {
                                intent.putExtra("position", Utility.getPosition("park"));
                                intent.putExtra("latlng", latlng[0] + "," + latlng[1]);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getActivity(), "Coming Soon", Toast.LENGTH_SHORT).show();
                            }
                        }
                        // Post to Main Thread
                        mThreadHandler.sendEmptyMessage(1);
                    }
                }, 500);

            }
        });

        if (mThreadHandler == null) {
            // Initialize and start the HandlerThread
            // which is basically a Thread with a Looper
            // attached (hence a MessageQueue)
            mHandlerThread = new HandlerThread(TAG, android.os.Process.THREAD_PRIORITY_BACKGROUND);
            mHandlerThread.start();

            // Initialize the Handler
            mThreadHandler = new Handler(mHandlerThread.getLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == 1) {
                        ArrayList<AutocompletePlace> results = mAdapter.resultList;

                        if (results != null && results.size() > 0) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                        else {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyDataSetInvalidated();
                                }
                            });
                        }
                    }
                }
            };
        }
        mMaterialSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String value = s.toString();

                // Remove all callbacks and messages
                mThreadHandler.removeCallbacksAndMessages(null);

                // Now add a new one
                mThreadHandler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // Background thread
                        mAdapter.resultList = mAdapter.mPlaceAPI.autocomplete(value);

                        // Footer
                        if (mAdapter.resultList.size() > 0)
                            mAdapter.resultList.add(new AutocompletePlace("footer", ""));

                        // Post to Main Thread
                        mThreadHandler.sendEmptyMessage(1);
                    }
                }, 500);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


//        mMaterialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                Log.v("QUERY ITEM : ", query);
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                mNameyet = newText;
//                if (!mNameyet.contains(" ") && mNameyet.length() % 3 == 0) {
//                    cityAutoComplete();
//                }
//                return true;
//            }
//        });

        mDatabase = AppDataBase.getAppDatabase(mActivity);



        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCities.clear();
//        mCities = new ArrayList<>(Arrays.asList(mDatabase.cityDao().loadAll()));
        mInterests = new ArrayList<>(Arrays.asList(
                mActivity.getString(R.string.interest_know_more)
//                mActivity.getString(R.string.interest_weather),
//                mActivity.getString(R.string.interest_fun_facts),
//                mActivity.getString(R.string.interest_trends)
        ));

//        if (checkCachedCities(mCities)) {
            fetchCitiesList();
//        } else {
//            animationView.setVisibility(View.GONE);
//
//            for (City city : mCities)
//                city.mInterests = mInterests;
//
//            mCityAdapter.updateData(mCities);
//
//            if (mSpotlightShownCount <= 3) {
//                showSpotlightView(mSpotView);
//            }
//        }
    }

    /**
     * shows spotlight on city card for the first 3 times.
     *
     * @param spotView - the view to be highlighted
     */
    private void showSpotlightView(View spotView) {
        CustomTarget customTarget = new CustomTarget.Builder(getActivity())
                .setPoint(180f, 430f)
                .setShape(new Circle(200f))
                .setOverlay(spotView)
                .setOnSpotlightStartedListener(new OnTargetStateChangedListener<CustomTarget>() {
                    @Override
                    public void onStarted(CustomTarget target) {
                        // do something
                    }

                    @Override
                    public void onEnded(CustomTarget target) {
                        // do something
                    }
                })
                .build();


        Spotlight spotlight =
                Spotlight.with(mActivity)
                        .setOverlayColor(R.color.spotlight)
                        .setDuration(1000L)
                        .setAnimation(new DecelerateInterpolator(2f))
                        .setTargets(customTarget)
                        .setClosedOnTouchedOutside(false)
                        .setOnSpotlightStateListener(new OnSpotlightStateChangedListener() {
                            @Override
                            public void onStarted() {
                                //do something
                            }

                            @Override
                            public void onEnded() {
                                //do something
                            }
                        });
        spotlight.start();

        View.OnClickListener closeSpotlight = v -> {
            spotlight.closeSpotlight();
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putInt(SPOTLIGHT_SHOW_COUNT, mSpotlightShownCount + 1);
            editor.apply();
        };

        spotView.findViewById(R.id.close_spotlight).setOnClickListener(closeSpotlight);
    }

    /**
     * Check cached cities with expiration time 24 hours
     *
     * @param mCities - list of cities object
     **/
    private boolean checkCachedCities(List<City> mCities) {
        return mCities.size() == 0 || is24Hours();
    }

    /**
     * Check time more than 24 hours
     **/
    private boolean is24Hours() {
        long mMillisPerDay = 24 * 60 * 60 * 1000L;
        boolean mMoreThanDay = false;
        Date mCurrentDate = new Date();
        try {
            String mDateString = mSharedPreferences.getString(LAST_CACHE_TIME, "");
            if (!mDateString.isEmpty()) {
                Date mExpiry = mFormat.parse(mDateString);
                mMoreThanDay = Math.abs(mCurrentDate.getTime() - mExpiry.getTime()) > mMillisPerDay;
                //Delete record cached before 24 hours
                if (mMoreThanDay) {
                    mDatabase.cityDao().deleteAll();
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mMoreThanDay;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mActivity.getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
//        mMaterialSearchView.setMenuItem(item);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
//                view.findViewById(R.id.search_layout).setVisibility(View.VISIBLE);
                layout.setVisibility(View.VISIBLE);

                return true;
            }
        });
    }


    /**
     * Fetches the list of popular cities from server
     */
    private void fetchCitiesList() {

        DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference().child("Places");
        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i = 0;
                for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    City city = new City("0",
                            snapshot.child("lat").getValue().toString(),
                            snapshot.child("lng").getValue().toString(),
                            -1,
                            mColors[i],
                            snapshot.child("img").getValue().toString(),
                            snapshot.child("name").getValue().toString(),
                            "");

                    mCities.add(city);
                    if(i < 8) {
                        i = i + 1;
                    }else{
                        i = 0;
                    }
                }
                animationView.setVisibility(View.GONE);
                for (City city : mCities)
                    city.mInterests = mInterests;

                mCityAdapter = new CityAdapter(mActivity, mCities, mSettings);

                lv.setAdapter(mCityAdapter);

//                if (mSpotlightShownCount <= 3) {
//                    showSpotlightView(mSpotView);
//                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.v(TAG, "city_fetch" + databaseError.toString());
            }
        });

    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.mActivity = (Activity) activity;
    }

    /**
     * Plays the network lost animation in the view
     */
    private void networkError() {
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
