package com.yuvix.tourustan.destinations.description;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static utils.Constants.API_LINK_V2;

/**
 * Created by niranjanb on 15/05/17.
 */

class FinalCityInfoPresenter {
    private final OkHttpClient mOkHttpClient;
    private FinalCityInfoView mFinalCityInfoView;

    FinalCityInfoPresenter() {
        mOkHttpClient = new OkHttpClient();
    }

    public void attachView(FinalCityInfoView finalCityInfoView) {
        mFinalCityInfoView = finalCityInfoView;
    }

    /**
     * Calls the API & fetch details of the weather of city with given name
     *
     * @param cityid the city id
     * @param token    authentication token
     */
    public void fetchCityWeather(String cityid, String token) {

    }


    /**
     * Calls the API & fetch details of city with given id
     *
     * @param cityid the city id
     * @param token    authentication token
     */
    public void fetchCityInfo(String cityid, String token) {


    }
}
