package objects;

public class AutocompletePlace {
    String name;
    String place_id;

    public AutocompletePlace() {
    }

    public AutocompletePlace(String name, String place_id) {
        this.name = name;
        this.place_id = place_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }
}
