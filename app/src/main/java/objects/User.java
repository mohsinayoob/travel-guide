package objects;

public class User {

    private String mUsername;
    private String mFirstName;
    private String mLastName;
    private String mPassword;
    private String mEmail;
    private int mId;
    private String mImage;
    private String mDateJoined;
    private String mStatus;
    private String mtoken;

    public User(String mUsername, String mFirstName, String mLastName,
                int mId, String mImage, String mDateJoined, String mStatus) {
        this.mUsername = mUsername;
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mId = mId;
        this.mImage = mImage;
        this.mDateJoined = mDateJoined;
        this.mStatus = mStatus;
    }

    public User(String mFirstName, String mLastName, String mPassword, String mUsername) {
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mPassword = mPassword;
        this.mUsername = mUsername;
    }

    public User(String mFirstName, String mLastName, String mPassword, String mUsername, String mtoken) {
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mPassword = mPassword;
        this.mUsername = mUsername;
        this.mtoken = mtoken;
    }

    public String getMtoken() {
        return mtoken;
    }

    public void setMtoken(String mtoken) {
        this.mtoken = mtoken;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public User(String firstName, String image) {
        this.mFirstName = firstName;
        this.mImage = image;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public int getId() {
        return mId;
    }

    public String getImage() {
        return mImage;
    }

    public String getDateJoined() {
        return mDateJoined;
    }

    public String getStatus() {
        return mStatus;
    }
}
